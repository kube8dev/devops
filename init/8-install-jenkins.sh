kubectl apply -f jenkins-pvc.yaml

helm install --name jenkins --set Persistence.ExistingClaim=jenkins --set Master.ServiceType=NodePort --set Master.NodePort=30808 --namespace devops stable/jenkins

printf $(kubectl get secret --namespace devops jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode);echo

kubectl create rolebinding sa-devops-role-clusteradmin --clusterrole=cluster-admin --serviceaccount=devops:default --namespace=devops

kubectl create rolebinding sa-devops-role-clusteradmin-kubesystem --clusterrole=cluster-admin --serviceaccount=devops:default --namespace=kube-system