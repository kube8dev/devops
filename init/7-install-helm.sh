curl https://raw.githubusercontent.com/helm/helm/master/scripts/get > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
# helm init
kubectl apply -f tyller-sa.yaml
helm init --service-account tiller
kubectl patch deployment tiller-deploy -n kube-system --patch "$(cat tyller-patch.yaml)"
kubectl get deploy tiller-deploy -n kube-system -o yaml | grep Account