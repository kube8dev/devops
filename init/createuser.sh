mkdir ~/cert-user-{user}; cd ~/cert-user-{user}
openssl genrsa -out {user}.key 2048
openssl req -new -key {user}.key -out {user}.csr -subj “/CN={user}/O=developer” -days 365
openssl x509 -req -in {user}.csr -CA /etc/kubernetes/pki/ca.crt -CAkey /etc/kubernetes/pki/ca.key -CAcreateserial -out {user}.crt -days 365

kubectl config --kubeconfig=becaster-contexts set-cluster becaster-dev --server=https://13.66.227.41:6443 --certificate-authority="C:\certs\kubernetes.becaster.dev.crt"
kubectl config --kubeconfig=becaster-contexts set-credentials apereira --client-certificate="C:\certs\apereira.crt" --client-key="C:\certs\apereira.key"
kubectl config --kubeconfig=becaster-contexts set-context apereira-dev --cluster=becaster-dev --namespace=staging --user=apereira
export KUBECONFIG=~/.kube/becaster-contexts


# developer-staging-role.yaml

kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: developer
  namespace: staging
rules:
- apiGroups: [""] # vazio "" = core API group
  resources: ["deployments", "replicasets", "pods", "configmaps"] # nao acessa as secrets
  verbs: ["get", "list", "watch", "create", "update", "patch", "delete"] # Ou simplesmente ["*"]

kubectl apply -f developer-staging-role.yaml

# developer-staging-rolebinding.yaml

kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: developers-staging
  namespace: staging
subjects:
- kind: User
  name: {user}
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: developer
  apiGroup: rbac.authorization.k8s.io

kubectl apply -f developer-staging-rolebinding.yaml