podTemplate(
    label: 'kube8', 
    containers: [
        containerTemplate(args: 'cat', name: 'docker', command: '/bin/sh -c', image: 'docker', ttyEnabled: true),
        containerTemplate(args: 'cat', name: 'helm', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v2.11.0', ttyEnabled: true)
    ],
    volumes: [
      hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
    ]
){
    // Start pipeline
    node('kube8') {
        def REPOS
        def IMAGE_VERSION
		def KUBE_NAMESPACE
        def IMAGE_NAME = "kube8-frontend"
        def ENVIRONMENT = "staging"
        def GIT_REPO_URL = "git@gitlab.com:kube8dev/frontend.git"
		def GIT_BRANCH 
        def CHARTMUSEUM_REPO_URL = "http://helm-chartmuseum:8080"

        stage('Checkout') {
            echo 'Iniciando clone do repositorio'
			REPOS = checkout([$class: 'GitSCM', branches: [[name: '*/master'], [name: '*/develop']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab', url: GIT_REPO_URL]]])
            
			GIT_BRANCH = REPOS.GIT_BRANCH
			if(GIT_BRANCH.equals('master')){
				KUBE_NAMESPACE = "prod"
			} else if(GIT_BRANCH.equals('develop')){
				KUBE_NAMESPACE = "staging"
			} else {
				echo "Não existe pipeline para ${GIT_BRANCH}"
				exit 0
			}
			
			IMAGE_VERSION = sh returnStdout: true, script: 'sh read-package-version.sh'
            IMAGE_VERSION = IMAGE_VERSION.trim()
            echo IMAGE_VERSION
        }
        stage("Package") {
            container('docker') {
                echo 'Iniciando empacotamento com Docker'
                withCredentials([usernamePassword(credentialsId: 'docker', passwordVariable: 'DOCKER_HUB_PSSWD', usernameVariable: 'DOCKER_HUB_USER')]) {
                    sh "echo '${DOCKER_HUB_PSSWD}' | docker login --username ${DOCKER_HUB_USER} --password-stdin"
                    sh 'ls'
                    sh "docker build -t ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION} . --build-arg NPM_ENV='${ENVIRONMENT}'"
                    sh "docker push ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION}"
                }
            }
        }
        stage("Deploy"){
            container("helm") {
                echo 'Iniciando deploy com helm'
                sh "helm init --client-only"
                sh "helm repo add kube8 ${CHARTMUSEUM_REPO_URL}"
                sh "helm repo list"
                sh "helm repo update"
				try {
                	sh "helm upgrade --namespace=${KUBE_NAMESPACE} kube8-staging-frontend kube8/frontend --set image.tag=${IMAGE_VERSION}"
				} catch(Exception ex) {
                	sh "helm install --namespace=${KUBE_NAMESPACE} --name kube8-staging-frontend kube8/frontend --set image.tag=${IMAGE_VERSION}"					
				}
			}
        }
    }
}